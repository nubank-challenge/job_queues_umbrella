defmodule JobQueuesWeb.JobViewTest do
  use JobQueuesWeb.ConnCase, async: true

  alias JobQueuesWeb.JobView

  describe "assigned_job.json" do
    test "processing nil" do
      expected_result = %{assigned_job: nil}
      assert JobView.render("assigned_job.json", %{assigned_job: nil}) == expected_result
    end

    test "processing a job" do
      job = %{"id" => 1, "type" => "teste 1", "agent_id" => "1"}
      expected_result = %{assigned_job: job}
      assert JobView.render("assigned_job.json", %{assigned_job: job}) == expected_result
    end
  end

  describe "jobs_state.json" do
    test "processing a map with empty lists" do
      jobs_state = %{unassigned_urgent_jobs: [], unassigned_jobs: [], completed_jobs: [], assigned_jobs: []}
      expected_result = %{unassigned_jobs: [], assigned_jobs: [], completed_jobs: []}
      assert JobView.render("jobs_state.json", jobs_state) == expected_result
    end

    test "processing a map with list of jobs" do
      jobs_state = %{
        unassigned_urgent_jobs: [%{"id" => 2, "type" => "teste 2", "urgent" => true}],
        unassigned_jobs: [%{"id" => 1, "type" => "teste 1", "urgent" => false}],
        completed_jobs: [%{"id" => 4, "type" => "teste 3", "urgent" => true}],
        assigned_jobs: [%{"id" => 3, "type" => "teste 1", "urgent" => false}]
      }

      expected_result = %{unassigned_jobs: [%{"id" => 2, "type" => "teste 2", "urgent" => true}, %{"id" => 1, "type" => "teste 1", "urgent" => false}], assigned_jobs: [%{"id" => 3, "type" => "teste 1", "urgent" => false}], completed_jobs: [%{"id" => 4, "type" => "teste 3", "urgent" => true}]}
      assert JobView.render("jobs_state.json", jobs_state) == expected_result
    end
  end
end
