defmodule JobQueuesWeb.ErrorViewTest do
  use JobQueuesWeb.ConnCase, async: true

  alias JobQueuesWeb.ErrorView

  # Bring render/3 and render_to_string/3 for testing custom views
  import Phoenix.View

  test "renders 404.html" do
    assert render_to_string(ErrorView, "404.html", []) == "Page not found"
  end

  test "render 500.html" do
    assert render_to_string(ErrorView, "500.html", []) == "Internal server error"
  end

  test "render any other" do
    assert render_to_string(ErrorView, "505.html", []) == "Internal server error"
  end

  describe "errors.json" do
    test "render for invalid_agent error" do
      expected_result = %{errors: [%{error_message: "Invalid Agent information"}]}
      assert ErrorView.render("errors.json", %{error: :invalid_agent}) == expected_result
    end

    test "render for invalid_agent_id error" do
      expected_result = %{errors: [%{error_message: "The informed Agent ID is invalid"}]}
      assert ErrorView.render("errors.json", %{error: :invalid_agent_id}) == expected_result
    end

    test "render for agent_not_found error" do
      expected_result = %{errors: [%{error_message: "Agent not found"}]}
      assert ErrorView.render("errors.json", %{error: :agent_not_found}) == expected_result
    end

    test "render for fittest_job_not_found error" do
      expected_result = %{errors: [%{error_message: "No suitable job was found"}]}
      assert ErrorView.render("errors.json", %{error: :fittest_job_not_found}) == expected_result
    end

    test "render for invalid_job error" do
      expected_result = %{errors: [%{error_message: "Invalid Job information"}]}
      assert ErrorView.render("errors.json", %{error: :invalid_job}) == expected_result
    end

    test "render for invalid_job_id error" do
      expected_result = %{errors: [%{error_message: "The informed Job ID is invalid"}]}
      assert ErrorView.render("errors.json", %{error: :invalid_job_id}) == expected_result
    end

    test "render for job_already_exists error" do
      expected_result = %{errors: [%{error_message: "A job with the informed ID already exists"}]}
      assert ErrorView.render("errors.json", %{error: :job_already_exists}) == expected_result
    end

    test "render for not mapped error error" do
      expected_result = %{errors: [%{error_message: "Other_error"}]}
      assert ErrorView.render("errors.json", %{error: :other_error}) == expected_result
    end

    test "render for changeset without errors" do
      expected_result = %{errors: []}
      assert ErrorView.render("errors.json", %{changeset: %{errors: []}}) == expected_result
    end

    test "render for changeset with errors" do
      expected_result = %{errors: [%{attribute: :id, error_message: "Can't be blank"}]}
      assert ErrorView.render("errors.json", %{changeset: %{errors: [id: {"can't be blank", [validation: :required]}]}}) == expected_result
    end
  end
end
