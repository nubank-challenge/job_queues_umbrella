defmodule JobQueuesWeb.AgentViewTest do
  use JobQueuesWeb.ConnCase, async: true

  alias JobQueuesWeb.AgentView

  describe "agent_stats.json" do
    test "processing empty list" do
      expected_result = %{completed_jobs: %{}}
      assert AgentView.render("agent_stats.json", %{completed_jobs: []}) == expected_result
    end

    test "processing list of completed jobs" do
      completed_jobs = [
        %{"id" => 1, "type" => "teste 1", "agent_id" => "1"},
        %{"id" => 2, "type" => "teste 2", "agent_id" => "1"},
        %{"id" => 3, "type" => "teste 1", "agent_id" => "1"},
        %{"id" => 4, "type" => "teste 3", "agent_id" => "1"},
        %{"id" => 5, "type" => "teste 1", "agent_id" => "1"},
        %{"id" => 6, "type" => "teste 3", "agent_id" => "1"}
      ]

      expected_result = %{completed_jobs: %{"teste 1" => 3, "teste 2" => 1, "teste 3" => 2}}
      assert AgentView.render("agent_stats.json", %{completed_jobs: completed_jobs}) == expected_result
    end
  end
end
