defmodule JobQueuesWeb.AgentControllerTest do
  use JobQueuesWeb.ConnCase

  alias JobQueuesWeb.{ErrorView}

  setup do
    JobQueues.Agent.clean()
    JobQueues.Job.clean()
    :ok
  end

  describe "POST /agents" do
    test "renders a list of errors when don't pass required attribute", %{conn: conn} do
      conn = post(conn, agent_path(conn, :create), %{name: "Teste", primary_skillset: ["teste"], secondary_skillset: []})
      assert json_response(conn, 400) == render_json(ErrorView, "errors.json", %{changeset: %{errors: [{:id, "can't be blank"}]}})
    end

    test "renders a list of errors when pass wrong value type of attribute", %{conn: conn} do
      conn = post(conn, agent_path(conn, :create), %{id: "1", name: "Teste", primary_skillset: [1], secondary_skillset: []})
      assert json_response(conn, 400) == render_json(ErrorView, "errors.json", %{changeset: %{errors: [{:primary_skillset, "is invalid"}]}})
    end

    test "renders empty json when create a Agent", %{conn: conn} do
      conn = post(conn, agent_path(conn, :create), %{id: "1", name: "Teste", primary_skillset: ["teste"], secondary_skillset: []})
      assert json_response(conn, 201) == %{}
    end
  end

  describe "GET /agents/:agent_id/stats" do
    test "renders a list of errors when don't exist Agent with :agent_id", %{conn: conn} do
      conn = get(conn, agent_path(conn, :agent_stats, 1))
      assert json_response(conn, 404) == render_json(ErrorView, "errors.json", %{error: :agent_not_found})
    end

    test "renders a empty stats of completed jobs when exist Agent with :agent_id but don't exists completed jobs", %{conn: conn} do
      post(conn, agent_path(conn, :create), %{id: "1", name: "Teste", primary_skillset: ["teste"], secondary_skillset: []})

      conn = get(conn, agent_path(conn, :agent_stats, 1))
      assert json_response(conn, 200) == %{"completed_jobs" => %{}}
    end

    test "renders a stats of completed jobs when exist Agent with :agent_id and exists completed jobs", %{conn: conn} do
      post(conn, agent_path(conn, :create), %{id: "1", name: "Teste", primary_skillset: ["teste"], secondary_skillset: []})

      JobQueues.Job.insert_in_classification(1, %{"id" => 1, "type" => "teste 1", "agent_id" => "1"}, :completed_jobs)
      JobQueues.Job.insert_in_classification(2, %{"id" => 2, "type" => "teste 2", "agent_id" => "1"}, :completed_jobs)
      JobQueues.Job.insert_in_classification(3, %{"id" => 3, "type" => "teste 1", "agent_id" => "1"}, :completed_jobs)
      JobQueues.Job.insert_in_classification(4, %{"id" => 4, "type" => "teste 3", "agent_id" => "1"}, :completed_jobs)
      JobQueues.Job.insert_in_classification(5, %{"id" => 5, "type" => "teste 1", "agent_id" => "1"}, :completed_jobs)
      JobQueues.Job.insert_in_classification(6, %{"id" => 6, "type" => "teste 3", "agent_id" => "1"}, :completed_jobs)

      conn = get(conn, agent_path(conn, :agent_stats, 1))
      assert json_response(conn, 200) == %{"completed_jobs" => %{"teste 1" => 3, "teste 2" => 1, "teste 3" => 2}}
    end
  end
end
