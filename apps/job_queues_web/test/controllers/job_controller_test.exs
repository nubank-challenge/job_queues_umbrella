defmodule JobQueuesWeb.JobControllerTest do
  use JobQueuesWeb.ConnCase

  alias JobQueuesWeb.{ErrorView, JobView}

  setup do
    JobQueues.Agent.clean()
    JobQueues.Job.clean()
    :ok
  end

  describe "POST /jobs" do
    test "renders a list of errors when don't pass required attribute", %{conn: conn} do
      conn = post(conn, job_path(conn, :create), %{type: "teste", urgent: false})
      assert json_response(conn, 400) == render_json(ErrorView, "errors.json", %{changeset: %{errors: [{:id, "can't be blank"}]}})
    end

    test "renders a list of errors when pass wrong value type of attribute", %{conn: conn} do
      conn = post(conn, job_path(conn, :create), %{id: 1, type: "teste", urgent: false})
      assert json_response(conn, 400) == render_json(ErrorView, "errors.json", %{changeset: %{errors: [{:id, "is invalid"}]}})
    end

    test "renders empty json when create a Job", %{conn: conn} do
      conn = post(conn, job_path(conn, :create), %{id: "1", type: "teste", urgent: false})
      assert json_response(conn, 201) == %{}
    end

    test "renders a list of errors when pass job already existing", %{conn: conn} do
      post(conn, job_path(conn, :create), %{id: "1", type: "teste", urgent: false})
      conn = post(conn, job_path(conn, :create), %{id: "1", type: "teste", urgent: false})
      assert json_response(conn, 400) == render_json(ErrorView, "errors.json", %{error: :job_already_exists})
    end
  end

  describe "POST /job_request" do
    test "renders a list of errors when don't pass required attribute", %{conn: conn} do
      conn = post(conn, job_path(conn, :job_request), %{})
      assert json_response(conn, 400) == render_json(ErrorView, "errors.json", %{changeset: %{errors: [{:agent_id, "can't be blank"}]}})
    end

    test "renders a list of errors when pass wrong value type of attribute", %{conn: conn} do
      conn = post(conn, job_path(conn, :job_request), %{agent_id: 1})
      assert json_response(conn, 400) == render_json(ErrorView, "errors.json", %{changeset: %{errors: [{:agent_id, "is invalid"}]}})
    end

    test "renders a list of errors when don't exist Agent with :agent_id", %{conn: conn} do
      conn = post(conn, job_path(conn, :job_request), %{agent_id: "1"})
      assert json_response(conn, 404) == render_json(ErrorView, "errors.json", %{error: :agent_not_found})
    end

    test "renders a list of errors when don't find fittest job", %{conn: conn} do
      post(conn, agent_path(conn, :create), %{id: "1", name: "Teste", primary_skillset: ["teste 2"], secondary_skillset: []})
      post(conn, job_path(conn, :create), %{id: "1", type: "teste", urgent: false})

      conn = post(conn, job_path(conn, :job_request), %{agent_id: "1"})
      assert json_response(conn, 404) == render_json(ErrorView, "errors.json", %{error: :fittest_job_not_found})
    end

    test "renders the assigned job when find fittest job", %{conn: conn} do
      assigned_at = DateTime.utc_now
      :meck.new(DateTime, [:passthrough])
      :meck.sequence(DateTime, :utc_now, 0, [assigned_at])
      expected_assigned_job = %{id: "1", type: "teste", urgent: false, agent_id: "1", assigned_at: assigned_at}

      post(conn, agent_path(conn, :create), %{id: "1", name: "Teste", primary_skillset: ["teste"], secondary_skillset: []})
      post(conn, job_path(conn, :create), %{id: "1", type: "teste", urgent: false})

      conn = post(conn, job_path(conn, :job_request), %{agent_id: "1"})
      assert json_response(conn, 201) == render_json(JobView, "assigned_job.json", %{assigned_job: expected_assigned_job})
    end
  end

  describe "GET /jobs_state" do
    test "renders a empty list for each job situation", %{conn: conn} do
      conn = get(conn, job_path(conn, :jobs_state))
      assert json_response(conn, 200) == %{"assigned_jobs" => [], "completed_jobs" => [], "unassigned_jobs" => []}
    end

    test "renders a list for each job situation", %{conn: conn} do
      unassigned_job = %{"id" => 1, "type" => "teste 1", "urgent" => false}
      unassigned_urgent_job = %{"id" => 2, "type" => "teste 2", "urgent" => true}
      assigned_job = %{"id" => 3, "type" => "teste 1", "urgent" => false}
      completed_job = %{"id" => 4, "type" => "teste 3", "urgent" => true}

      JobQueues.Job.insert(unassigned_job)
      JobQueues.Job.insert(unassigned_urgent_job)
      JobQueues.Job.insert_in_classification(3, assigned_job, :assigned_jobs)
      JobQueues.Job.insert_in_classification(4, completed_job, :completed_jobs)

      expected_response = %{
        "assigned_jobs" => [assigned_job],
        "completed_jobs" => [completed_job],
        "unassigned_jobs" => [unassigned_urgent_job, unassigned_job]
      }

      conn = get(conn, job_path(conn, :jobs_state))
      assert json_response(conn, 200) == expected_response
    end
  end
end
