defmodule JobQueuesWeb.JobController do
  use JobQueuesWeb.Web, :controller

  alias JobQueuesWeb.ErrorView
  alias JobQueues.Job

  @not_fount_errors [:agent_not_found, :fittest_job_not_found]

  def create(conn, params) do
    changeset = job_changeset(params)
    if changeset.valid? do
      params = changes_to_params(changeset.changes)
      case Job.insert(params) do
        :ok ->
          conn
          |> put_status(:created)
          |> json(%{})
        {:error, error} ->
          conn
          |> put_status(:bad_request)
          |> put_view(ErrorView)
          |> render("errors.json", %{error: error})
      end
    else
      conn
      |> put_status(:bad_request)
      |> put_view(ErrorView)
      |> render("errors.json", %{changeset: changeset})
    end
  end

  def job_request(conn, params) do
    changeset = job_request_changeset(params)
    if changeset.valid? do
      agent_id = Map.get(params, "agent_id")
      case Job.Requester.request(agent_id) do
        :ok ->
          assigned_job = Job.find_assigned_job_by_agent_id(agent_id)
          conn
          |> put_status(:created)
          |> render("assigned_job.json", %{assigned_job: assigned_job})
        {:error, error} when error in @not_fount_errors ->
          conn
          |> put_status(:not_found)
          |> put_view(ErrorView)
          |> render("errors.json", %{error: error})
        {:error, error} ->
          conn
          |> put_status(:bad_request)
          |> put_view(ErrorView)
          |> render("errors.json", %{error: error})
      end
    else
      conn
      |> put_status(:bad_request)
      |> put_view(ErrorView)
      |> render("errors.json", %{changeset: changeset})
    end
  end

  def jobs_state(conn, _params), do: render(conn, "jobs_state.json", Job.all())
end
