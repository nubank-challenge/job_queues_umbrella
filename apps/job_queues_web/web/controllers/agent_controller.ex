defmodule JobQueuesWeb.AgentController do
  use JobQueuesWeb.Web, :controller

  alias JobQueuesWeb.ErrorView
  alias JobQueues.{Agent, Job}

  def create(conn, params) do
    changeset = agent_changeset(params)
    if changeset.valid? do
      params = changes_to_params(changeset.changes)
      case Agent.insert_or_update(params) do
        :ok ->
          conn
          |> put_status(:created)
          |> json(%{})
        {:error, error} ->
          conn
          |> put_status(:bad_request)
          |> put_view(ErrorView)
          |> render("errors.json", %{error: error})
      end
    else
      conn
      |> put_status(:bad_request)
      |> put_view(ErrorView)
      |> render("errors.json", %{changeset: changeset})
    end
  end

  def agent_stats(conn, %{"agent_id" => agent_id}) do
    case Agent.find(agent_id) do
      nil ->
        conn
        |> put_status(:not_found)
        |> put_view(ErrorView)
        |> render("errors.json", %{error: :agent_not_found})
      _agent ->
        completed_jobs = Job.find_completed_jobs_by_agent_id(agent_id)
        render(conn, "agent_stats.json", %{completed_jobs: completed_jobs})
    end
  end
end
