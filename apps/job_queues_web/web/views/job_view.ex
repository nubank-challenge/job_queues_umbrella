defmodule JobQueuesWeb.JobView do
  use JobQueuesWeb.Web, :view

  def render("assigned_job.json", %{assigned_job: assigned_job}) do
    %{assigned_job: assigned_job}
  end

  def render("jobs_state.json", jobs_state) do
    %{
      unassigned_urgent_jobs: unassigned_urgent_jobs,
      unassigned_jobs: unassigned_jobs,
      completed_jobs: completed_jobs,
      assigned_jobs: assigned_jobs
    } = jobs_state
    
    %{
      unassigned_jobs: unassigned_urgent_jobs ++ unassigned_jobs,
      assigned_jobs: assigned_jobs,
      completed_jobs: completed_jobs
    }
  end
end