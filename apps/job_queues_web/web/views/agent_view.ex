defmodule JobQueuesWeb.AgentView do
  use JobQueuesWeb.Web, :view

  def render("agent_stats.json", %{completed_jobs: completed_jobs}) do
    %{completed_jobs: extract_stats_from_completed_jobs(completed_jobs)}
  end

  defp extract_stats_from_completed_jobs(completed_jobs) do
    Enum.reduce(completed_jobs, %{}, fn (%{"type" => type}, acc) ->
      Map.update(acc, type, 1, &(&1 + 1))
    end)
  end
end