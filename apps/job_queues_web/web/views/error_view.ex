defmodule JobQueuesWeb.ErrorView do
  use JobQueuesWeb.Web, :view

  # In case no render clause matches or no
  # template is found, let's render it as 500
  def template_not_found(_template, assigns) do
    render "500.html", assigns
  end

  def render("404.html", _assigns) do
    "Page not found"
  end

  def render("500.html", _assigns) do
    "Internal server error"
  end

  def render("errors.json", %{changeset: changeset}) do
    errors = Enum.map(changeset.errors, fn {field, detail} ->
      %{attribute: field, error_message: render_error_message(detail)}
    end)

    %{errors: errors}
  end

  def render("errors.json", %{error: error}) do
    %{errors: [%{error_message: render_error_message(error)}]}
  end

  defp render_error_message({message, values}) do
    Enum.reduce(values, message, fn ({key, value}, acc) -> safe_replace(acc, key, value) end) |> String.capitalize()
  end

  defp render_error_message(:invalid_agent), do: "Invalid Agent information"
  defp render_error_message(:invalid_agent_id), do: "The informed Agent ID is invalid"
  defp render_error_message(:agent_not_found), do: "Agent not found"
  defp render_error_message(:fittest_job_not_found), do: "No suitable job was found"
  defp render_error_message(:invalid_job), do: "Invalid Job information"
  defp render_error_message(:invalid_job_id), do: "The informed Job ID is invalid"
  defp render_error_message(:job_already_exists), do: "A job with the informed ID already exists"
  defp render_error_message(message), do: to_string(message) |> String.capitalize()

  defp safe_replace(acc, _key, value) when is_tuple(value), do: acc
  defp safe_replace(acc, key, value), do: String.replace(acc, "%{#{key}}", to_string(value))
end
