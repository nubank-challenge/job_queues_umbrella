defmodule JobQueuesWeb.Web do
  @moduledoc """
  The entrypoint for defining your web interface, such
  as controllers, views, channels and so on.

  This can be used in your application as:

      use JobQueuesWeb.Web, :controller
      use JobQueuesWeb.Web, :view

  The definitions below will be executed for every view,
  controller, etc, so keep them short and clean, focused
  on imports, uses and aliases.

  Do NOT define functions inside the quoted expressions
  below. Instead, define any helper function in modules
  and import those modules here.
  """

  def model do
    quote do
      # Define common model functionality
    end
  end

  def controller do
    quote do
      use Phoenix.Controller
      use Params

      import JobQueuesWeb.Router.Helpers
      import JobQueuesWeb.Gettext

      defp changes_to_params(params) do
        for {k, v} <- params, into: %{}, do: {Atom.to_string(k), v}
      end

      defparams agent_changeset %{
        id!: :string,
        name!: :string,
        primary_skillset!: {:array, :string},
        secondary_skillset!: {:array, :string}
      }

      defparams job_changeset %{
        id!: :string,
        type!: :string,
        urgent!: :boolean
      }

      defparams job_request_changeset %{
        agent_id!: :string
      }
    end
  end

  def view do
    quote do
      use Phoenix.View, root: "web/templates"

      # Import convenience functions from controllers
      import Phoenix.Controller, only: [get_flash: 2, view_module: 1]

      # Use all HTML functionality (forms, tags, etc)
      use Phoenix.HTML

      import JobQueuesWeb.Router.Helpers
      import JobQueuesWeb.ErrorHelpers
      import JobQueuesWeb.Gettext
    end
  end

  def router do
    quote do
      use Phoenix.Router
    end
  end

  def channel do
    quote do
      use Phoenix.Channel
      import JobQueuesWeb.Gettext
    end
  end

  @doc """
  When used, dispatch to the appropriate controller/view/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
