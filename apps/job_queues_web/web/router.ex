defmodule JobQueuesWeb.Router do
  use JobQueuesWeb.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", JobQueuesWeb do
    pipe_through :api

    resources "/agents", AgentController, only: [:create]
    get  "/agents/:agent_id/stats", AgentController, :agent_stats

    resources "/jobs", JobController, only: [:create]
    post "/job_request", JobController, :job_request
    get "/jobs_state", JobController, :jobs_state
  end
end
