# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :job_queues_web, JobQueuesWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "LSJkFGrcGxC7TIFMgGRO2jfLVdME4IuHp9J7DVf/g4agulEVZGyFiPjhBDvqVeO3",
  render_errors: [view: JobQueuesWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: JobQueuesWeb.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
