# JobQueuesWeb

**Description**

This project was created for resolve Nubank challenge part 2. This challenge consists in create an HTTP API to interact with part 1 of this challenge (JobQueue), for:

- Add agent.
- Add job.
- Request job.
- Get current queue state.
- Get agent stats.

Obs: All endpoints accept and return JSON content type payloads.

This proposed solution receives and processes the requests:

*Add agent*:

```
POST http://localhost:4000/agents
```

Example input:

```json
{
  "id": "8ab86c18-3fae-4804-bfd9-c3d6e8f66260",
  "name": "BoJack Horseman",
  "primary_skillset": ["bills-questions"],
  "secondary_skillset": []
}
  ```

*Add job*:

```
POST http://localhost:4000/jobs
```

Example input:

```json
{
  "id": "f26e890b-df8e-422e-a39c-7762aa0bac36",
  "type": "rewards_question",
  "urgent": false
}
  ```

*Request job*:

```
POST http://localhost:4000/job_request
```

Example input:

```json
{
  "agent_id": "8ab86c18-3fae-4804-bfd9-c3d6e8f66260"
}
```

*Get current queue state*:

```
GET "http://localhost:4000/jobs_state"
```

*Get agent stats*:

```
GET "http://localhost:4000/agents/:agent_id/stats"
```

**Setup and Using**

This project was created using Elixir 1.5.2, to install follow the [instructions](https://elixir-lang.org/install.html) of official Elixir site.
Phoenix requires Node.js with npm, to install follow the [instructions](https://nodejs.org/en/download/package-manager/).

To start the Phoenix app:
  * Install dependencies with `mix deps.get`
  * Install Node.js dependencies with `npm install`
  * Start Phoenix endpoint with `mix phoenix.server`

Now you can make requests using a HTTP Client, like a [Postman](https://www.getpostman.com/).

**Tests**

Run `mix test` on the terminal to run the  tests for this project.
```bash
$ mix test
....................................

Finished in 0.3 seconds
36 tests, 0 failures

Randomized with seed 644533
```