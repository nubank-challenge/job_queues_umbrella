defmodule JobQueues.JobTest do
  use ExUnit.Case, async: false

  setup do
    JobQueues.Job.clean()
  end

  describe ".insert/1" do
    test "should answer with error when try insert job with nil id" do
      assert JobQueues.Job.find(1) == %{}
      assert JobQueues.Job.insert(%{"id" => nil, "name" => "Invalid"}) == {:error, :invalid_job_id}
      assert JobQueues.Job.find(1) == %{}
    end

    test "should answer with error when try insert job with invalid payload" do
      assert JobQueues.Job.find(1) == %{}
      assert JobQueues.Job.insert([{"id", 1}, {"name", "Invalid"}]) == {:error, :invalid_job}
      assert JobQueues.Job.find(1) == %{}
    end

    test "should answer with :ok when try insert urgent job with valid payload" do
      job_payload = %{"id" => 1, "urgent" => true, "type" => "Teste"}
      assert JobQueues.Job.find(1) == %{}
      assert JobQueues.Job.insert(job_payload) == :ok
      assert JobQueues.Job.find(1) == %{unassigned_urgent_jobs: job_payload}
    end

    test "should answer with :ok when try insert job with valid payload" do
      job_payload = %{"id" => 1, "urgent" => false, "type" => "Teste"}
      assert JobQueues.Job.find(1) == %{}
      assert JobQueues.Job.insert(job_payload) == :ok
      assert JobQueues.Job.find(1) == %{unassigned_jobs: job_payload}
    end

    test "should answer with error when job already exists" do
      job_payload = %{"id" => 1, "urgent" => false, "type" => "Teste"}
      assert JobQueues.Job.find(1) == %{}
      assert JobQueues.Job.insert(job_payload) == :ok
      assert JobQueues.Job.find(1) == %{unassigned_jobs: job_payload}
      assert JobQueues.Job.insert(job_payload) == {:error, :job_already_exists}
    end
  end

  describe ".insert_in_classification/3" do
    test "should answer with error when try insert job with nil id" do
      assert JobQueues.Job.find(1) == %{}
      assert JobQueues.Job.insert_in_classification(nil, %{"id" => nil, "name" => "Invalid"}, :assigned_jobs) == {:error, :invalid_job_id}
      assert JobQueues.Job.find(1) == %{}
    end

    test "should answer with :ok when try insert job with valid payload" do
      job_payload = %{"id" => 1, "urgent" => false, "type" => "Teste"}
      assert JobQueues.Job.find(1) == %{}
      assert JobQueues.Job.insert_in_classification(1, job_payload, :assigned_jobs) == :ok
      assert JobQueues.Job.find(1) == %{assigned_jobs: job_payload}
    end

    test "should answer with error when job already exists" do
      job_payload = %{"id" => 1, "urgent" => false, "type" => "Teste"}
      assert JobQueues.Job.find(1) == %{}
      assert JobQueues.Job.insert_in_classification(1, job_payload, :assigned_jobs) == :ok
      assert JobQueues.Job.find(1) == %{assigned_jobs: job_payload}
      assert JobQueues.Job.insert_in_classification(1, job_payload, :assigned_jobs) == {:error, :job_already_exists}
    end
  end

  describe ".find/1" do
    test "should answer with nil when try find job with nil id" do
      assert JobQueues.Job.find(nil) == %{}
    end

    test "should answer with nil when job don't exists" do
      assert JobQueues.Job.find(1) == %{}
    end

    test "should answer with a Map with job_classification and job payload when job exists" do
      job_payload = %{"id" => 1, "urgent" => false, "type" => "Teste"}
      assert JobQueues.Job.find(1) == %{}
      assert JobQueues.Job.insert(job_payload) == :ok
      assert JobQueues.Job.find(1) == %{unassigned_jobs: job_payload}
    end

    test "should answer with a Map with job_classification and job payload when job exists and is assigned" do
      job_payload = %{"id" => 1, "urgent" => false, "type" => "Teste"}
      assert JobQueues.Job.find(1) == %{}
      assert JobQueues.Job.insert_in_classification(10, job_payload, :assigned_jobs) == :ok
      assert JobQueues.Job.find(1) == %{assigned_jobs: job_payload}
    end
  end

  describe ".find_in_classification/2" do
    test "should answer with nil when try find job with nil id" do
      assert JobQueues.Job.find_in_classification(nil, :completed_jobs) == nil
    end

    test "should answer with nil when job don't exists" do
      assert JobQueues.Job.find_in_classification(1, :completed_jobs) == nil
    end

    test "should answer with job payload when job exists" do
      job_payload = %{"id" => 1, "urgent" => false, "type" => "Teste"}
      assert JobQueues.Job.find_in_classification(1, :completed_jobs) == nil
      assert JobQueues.Job.insert_in_classification(1, job_payload, :completed_jobs) == :ok
      assert JobQueues.Job.find_in_classification(1, :completed_jobs) == job_payload
    end
  end

  describe ".all/0" do
    test "should answer with a Map with empty lists when don't exists current jobs" do
      expected = %{assigned_jobs: [], completed_jobs: [], unassigned_jobs: [], unassigned_urgent_jobs: []}
      assert JobQueues.Job.all() == expected
    end

    test "should answer with a Map with lists when exists current jobs" do
      job_payload1 = %{"id" => 1, "urgent" => false, "type" => "Teste 1"}
      job_payload2 = %{"id" => 2, "urgent" => true, "type" => "Teste 2"}
      job_payload3 = %{"id" => 3, "urgent" => true, "type" => "Teste 3"}
      job_payload4 = %{"id" => 4, "urgent" => true, "type" => "Teste 4"}

      empty_expected = %{assigned_jobs: [], completed_jobs: [], unassigned_jobs: [], unassigned_urgent_jobs: []}
      filled_expected = %{assigned_jobs: [job_payload3], completed_jobs: [job_payload4], unassigned_jobs: [job_payload1], unassigned_urgent_jobs: [job_payload2]}

      assert JobQueues.Job.all() == empty_expected
      assert JobQueues.Job.insert(job_payload1) == :ok
      assert JobQueues.Job.insert(job_payload2) == :ok
      assert JobQueues.Job.insert_in_classification(3, job_payload3, :assigned_jobs) == :ok
      assert JobQueues.Job.insert_in_classification(4, job_payload4, :completed_jobs) == :ok
      assert JobQueues.Job.all() == filled_expected
    end
  end

  describe ".all/1" do
    test "should answer with empty lists when don't exists current jobs" do
      assert JobQueues.Job.all(:assigned_jobs) == []
    end

    test "should answer with list when exists current jobs" do
      job_payload1 = %{"id" => 1, "urgent" => false, "type" => "Teste 1"}
      job_payload2 = %{"id" => 2, "urgent" => true, "type" => "Teste 2"}
      job_payload3 = %{"id" => 3, "urgent" => true, "type" => "Teste 3"}
      job_payload4 = %{"id" => 4, "urgent" => true, "type" => "Teste 4"}

      assert JobQueues.Job.all(:assigned_jobs) == []
      assert JobQueues.Job.insert_in_classification(10, job_payload1, :assigned_jobs) == :ok
      assert JobQueues.Job.insert_in_classification(20, job_payload2, :assigned_jobs) == :ok
      assert JobQueues.Job.insert_in_classification(30, job_payload3, :assigned_jobs) == :ok
      assert JobQueues.Job.insert_in_classification(40, job_payload4, :assigned_jobs) == :ok
      assert JobQueues.Job.all(:assigned_jobs) == [job_payload1, job_payload2, job_payload3, job_payload4]
    end
  end

  describe ".find_unassigned_job_in_types/1" do
    test "should answer with a Map with value nil when don't exists jobs with match type" do
      expected = %{unassigned_jobs: nil, unassigned_urgent_jobs: nil}
      assert JobQueues.Job.find_unassigned_job_in_types(["Teste 1", "Teste 2"]) == expected
    end

    test "should answer with a Map with jobs when exists jobs with match type" do
      job_payload1 = %{"id" => 1, "urgent" => true, "type" => "Teste 1"}
      job_payload2 = %{"id" => 2, "urgent" => false, "type" => "Teste 2"}

      empty_expected = %{unassigned_jobs: nil, unassigned_urgent_jobs: nil}
      filled_expected = %{unassigned_jobs: job_payload2, unassigned_urgent_jobs: job_payload1}

      assert JobQueues.Job.find_unassigned_job_in_types(["Teste 1", "Teste 2"]) == empty_expected
      assert JobQueues.Job.insert(job_payload1) == :ok
      assert JobQueues.Job.insert(job_payload2) == :ok
      assert JobQueues.Job.find_unassigned_job_in_types(["Teste 1", "Teste 2"]) == filled_expected
    end
  end

  describe ".find_assigned_job_by_agent_id/1" do
    test "should answer with nil when try find assigned job with agent_id nil" do
      assert JobQueues.Job.find_assigned_job_by_agent_id(nil) == nil
    end

    test "should answer with nil when assigned job don't exists" do
      assert JobQueues.Job.find_assigned_job_by_agent_id(1) == nil
    end

    test "should answer with assigned job payload when assigned job exists" do
      job_payload = %{"id" => 1, "urgent" => false, "type" => "Teste"}
      assert JobQueues.Job.find_assigned_job_by_agent_id(1) == nil
      assert JobQueues.Job.insert_in_classification(1, job_payload, :assigned_jobs) == :ok
      assert JobQueues.Job.find_assigned_job_by_agent_id(1) == job_payload
    end
  end

  describe ".find_completed_jobs_by_agent_id/1" do
    test "should answer with [] when try find assigned job with agent_id nil" do
      assert JobQueues.Job.find_completed_jobs_by_agent_id(nil) == []
    end

    test "should answer with [] when don't exists done jobs" do
      assert JobQueues.Job.find_completed_jobs_by_agent_id(1) == []
    end

    test "should answer with [] when don't exists done jobs for agent_id" do
      job_payload = %{"id" => 1, "urgent" => false, "type" => "Teste", "agent_id" => 10}
      assert JobQueues.Job.find_completed_jobs_by_agent_id(1) == []
      assert JobQueues.Job.insert_in_classification(1, job_payload, :completed_jobs) == :ok
      assert JobQueues.Job.find_completed_jobs_by_agent_id(1) == []
    end

    test "should answer with a List of completed_jobs when exists done jobs for agent_id" do
      job_payload = %{"id" => 1, "urgent" => false, "type" => "Teste", "agent_id" => 10}
      assert JobQueues.Job.find_completed_jobs_by_agent_id(10) == []
      assert JobQueues.Job.insert_in_classification(1, job_payload, :completed_jobs) == :ok
      assert JobQueues.Job.find_completed_jobs_by_agent_id(10) == [job_payload]
    end
  end

  describe ".completes_assigned_job/2" do
    test "should answer with nil when try find completes assigned job with agent_id nil" do
      assert JobQueues.Job.completes_assigned_job(nil, 1) == {:error, :invalid_agent_id}
    end

    test "should answer with nil when try find completes assigned job with job nil" do
      assert JobQueues.Job.completes_assigned_job(10, nil) == {:error, :invalid_job}
    end

    test "should answer with nil when try find completes assigned job with job_id nil" do
      assert JobQueues.Job.completes_assigned_job(10, %{"id" => nil}) == {:error, :invalid_job_id}
    end

    test "should answer with nil when assigned job don't exists" do
      assert JobQueues.Job.completes_assigned_job(10, %{"id"=> 1}) == :ok
    end

    test "should answer with assigned job payload when assigned job exists" do
      agent_id = 10
      job_payload = %{"id" => 1, "urgent" => false, "type" => "Teste"}

      assert JobQueues.Job.find_in_classification(agent_id, :assigned_jobs) == nil
      assert JobQueues.Job.insert_in_classification(agent_id, job_payload, :assigned_jobs) == :ok
      assert JobQueues.Job.find_in_classification(agent_id, :assigned_jobs) == job_payload

      assert JobQueues.Job.completes_assigned_job(agent_id, job_payload) == :ok
      assert JobQueues.Job.find_in_classification(agent_id, :assigned_jobs) == nil
      assert JobQueues.Job.find_in_classification(1, :completed_jobs) == job_payload
    end
  end

  describe ".delete_in_classification/2" do
    test "should answer with nil when try delete job with nil id" do
      assert JobQueues.Job.delete_in_classification(nil, :completed_jobs) == nil
    end

    test "should answer with :ok when job don't exists" do
      assert JobQueues.Job.delete_in_classification(1, :completed_jobs) == :ok
    end

    test "should answer with :ok when job exists" do
      job_payload = %{"id" => 1, "urgent" => false, "type" => "Teste"}
      assert JobQueues.Job.find_in_classification(1, :unassigned_jobs) == nil
      assert JobQueues.Job.insert(job_payload) == :ok
      assert JobQueues.Job.find_in_classification(1, :unassigned_jobs) == job_payload

      assert JobQueues.Job.delete_in_classification(1, :unassigned_jobs) == :ok
      assert JobQueues.Job.find_in_classification(1, :unassigned_jobs) == nil
    end
  end

  describe ".clean/0" do
    test "should answer with :ok when try clean current empty jobs" do
      assert JobQueues.Job.clean() == :ok
    end

    test "should answer with :ok when try clean current jobs" do
      job_payload =  %{"id" => 1, "urgent" => false, "type" => "Teste"}
      assert JobQueues.Job.find(1) == %{}
      assert JobQueues.Job.insert(job_payload) == :ok
      assert JobQueues.Job.find(1) == %{unassigned_jobs: job_payload}
      assert JobQueues.Job.clean() == :ok
      assert JobQueues.Job.find(1) == %{}
    end
  end
end