defmodule JobQueues.Job.RequesterTest do
  use ExUnit.Case, async: false

  setup do
    JobQueues.Agent.clean()
    JobQueues.Job.clean()
  end

  describe ".request/1" do
    test "should answer with nil when agent_id is nil" do
      assert JobQueues.Job.Requester.request(nil) == {:error, :invalid_agent_id}
    end

    test "should answer with nil when agent don't exists" do
      assert JobQueues.Agent.find(1) == nil
      assert JobQueues.Job.Requester.request(1) == {:error, :agent_not_found}
    end

    test "should answer with nil when don't exists jobs" do
      assert JobQueues.Agent.find(1) == nil
      agent_payload = %{"id" => 1, "name" => "Teste", "primary_skillset" => ["teste 1"], "secondary_skillset" => ["teste 2"]}
      assert JobQueues.Agent.insert_or_update(agent_payload) == :ok
      assert JobQueues.Agent.find(1) == agent_payload

      assert JobQueues.Job.Requester.request(1) == {:error, :fittest_job_not_found}
    end

    test "should answer with nil when don't find fittest jobs" do
      assert JobQueues.Agent.find(10) == nil
      agent_payload = %{"id" => 10, "name" => "Teste", "primary_skillset" => ["teste 1"], "secondary_skillset" => ["teste 2"]}
      assert JobQueues.Agent.insert_or_update(agent_payload) == :ok
      assert JobQueues.Agent.find(10) == agent_payload

      job_payload = %{"id" => 1, "urgent" => false, "type" => "teste 3"}
      assert JobQueues.Job.find(1) == %{}
      assert JobQueues.Job.insert(job_payload) == :ok
      assert JobQueues.Job.find(1) == %{unassigned_jobs: job_payload}

      assert JobQueues.Job.Requester.request(10) == {:error, :fittest_job_not_found}
    end

    test "should answer with :ok when find fittest jobs" do
      assigned_at = DateTime.utc_now
      :meck.new(DateTime)
      :meck.sequence(DateTime, :utc_now, 0, [assigned_at])

      assert JobQueues.Agent.find(10) == nil
      agent_payload = %{"id" => 10, "name" => "Teste", "primary_skillset" => ["teste 1"], "secondary_skillset" => ["teste 2"]}
      assert JobQueues.Agent.insert_or_update(agent_payload) == :ok
      assert JobQueues.Agent.find(10) == agent_payload

      job_payload1 = %{"id" => 1, "urgent" => false, "type" => "teste 2"}
      job_payload2 = %{"id" => 2, "urgent" => true, "type" => "teste 1"}
      
      expected = 
        job_payload2
        |> Map.put("assigned_at", assigned_at)
        |> Map.put("agent_id", 10)

      assert JobQueues.Job.find(1) == %{}
      assert JobQueues.Job.find(2) == %{}
      assert JobQueues.Job.insert(job_payload1) == :ok
      assert JobQueues.Job.insert(job_payload2) == :ok
      assert JobQueues.Job.find(1) == %{unassigned_jobs: job_payload1}
      assert JobQueues.Job.find(2) == %{unassigned_urgent_jobs: job_payload2}

      assert JobQueues.Job.find_in_classification(10, :assigned_jobs) == nil
      assert JobQueues.Job.Requester.request(10) == :ok
      assert JobQueues.Job.find_in_classification(10, :assigned_jobs) == expected

      assert JobQueues.Job.all(:completed_jobs) == []
    end

    test "should answer with :ok when find fittest jobs and already exists assigned_job" do
      assigned_at = DateTime.utc_now
      completed_at = DateTime.utc_now
      already_assigned_at = DateTime.utc_now
      :meck.new(DateTime)
      :meck.sequence(DateTime, :utc_now, 0, [completed_at, assigned_at])

      assert JobQueues.Agent.find(10) == nil
      agent_payload = %{"id" => 10, "name" => "Teste", "primary_skillset" => ["teste 1"], "secondary_skillset" => ["teste 2"]}
      assert JobQueues.Agent.insert_or_update(agent_payload) == :ok
      assert JobQueues.Agent.find(10) == agent_payload

      assigned_job_payload = %{"id" => 2, "urgent" => true, "type" => "teste 1", "assigned_at" => already_assigned_at, "agent_id" => 10}
      job_payload = %{"id" => 1, "urgent" => false, "type" => "teste 2"}
      expected = 
        job_payload
        |> Map.put("assigned_at", assigned_at)
        |> Map.put("agent_id", 10)

      assert JobQueues.Job.find(1) == %{}
      assert JobQueues.Job.insert(job_payload) == :ok
      assert JobQueues.Job.find(1) == %{unassigned_jobs: job_payload}

      assert JobQueues.Job.find_in_classification(10, :assigned_jobs) == nil
      assert JobQueues.Job.insert_in_classification(10, assigned_job_payload, :assigned_jobs) == :ok
      assert JobQueues.Job.Requester.request(10) == :ok
      assert JobQueues.Job.find_in_classification(10, :assigned_jobs) == expected

      assert JobQueues.Job.all(:completed_jobs) == [Map.put(assigned_job_payload, "completed_at", completed_at)]
    end
  end
end