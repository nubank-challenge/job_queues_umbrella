defmodule JobQueuesTest do
  use ExUnit.Case, async: false
  doctest JobQueues

  setup do
    JobQueues.clean()
  end

  describe ".process_input" do
    test "process the sample-input.json and respond with the sample-output.json" do
      {:ok, expected_result} = File.read("test/fixtures/sample-output.json")
      assert JobQueues.process_input("sample-input.json", "inputs/", false) == expected_result
    end

    test "process input with jobs for secondary skillset" do
      {:ok, expected_result} = File.read("test/fixtures/secondary-skillset-output.json")
      assert JobQueues.process_input("secondary-skillset-input.json", "test/fixtures/", false) == expected_result
    end

    test "process input with more job requests than jobs" do
      {:ok, expected_result} = File.read("test/fixtures/job-requests-more-than-jobs-output.json")
      assert JobQueues.process_input("job-requests-more-than-jobs-input.json", "test/fixtures/", false) == expected_result
    end

    test "process input with many job requests of an agent" do
      {:ok, expected_result} = File.read("test/fixtures/many-job-requests-of-an-agent-output.json")
      assert JobQueues.process_input("many-job-requests-of-an-agent-input.json", "test/fixtures/", false) == expected_result
    end

    test "process input with many job requests of an agent without new job" do
      {:ok, expected_result} = File.read("test/fixtures/many-job-requests-of-an-agent-without-new-job-output.json")
      assert JobQueues.process_input("many-job-requests-of-an-agent-without-new-job-input.json", "test/fixtures/", false) == expected_result
    end
  end

  describe ".list_assigned_jobs" do
    test "by default print and returns :ok" do
      assert JobQueues.list_assigned_jobs() == :ok
    end

    test "print and returns :ok" do
      assert JobQueues.list_assigned_jobs(true) == :ok
    end

    test "returns a list" do
      assert JobQueues.list_assigned_jobs(false) == "[]"
    end
  end
end