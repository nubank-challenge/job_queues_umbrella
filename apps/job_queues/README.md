# JobQueues

**Description**

This project was created for resolve a Nubank challenge part 1. This challenge consists in receive agents, jobs and job requests following the rules:

- Jobs that arrived first should be assigned first, unless it has a "urgent" flag, in which case it has a higher
  priority.
- A job cannot be assigned to more than one agent at a time.
- An agent is not handed a job whose type is not among its skill sets.
- An agent only receives a job whose type is contained among its secondary skill set if no job from its primary
  skill set is available.

This proposed solution receives and processes the messages according to their type:

- "new_agent" -> Insert an agent.
- "new_job" -> Insert a job. Jobs are separated in classifications.
- "job_request" -> Searches for the fittest job for the agent, completes the currently assigned job from that agent, changes the job classification to assigned jobs.

After that, get the list of assigned jobs, sort by date, format for json and print to stdout.

**Setup**

This project was created using Elixir 1.5.2, to install follow the [instructions](https://elixir-lang.org/install.html) of official Elixir site.

After clone the repository and access it, run `mix deps.get` to get the dependencies of the project.

**Using**

Run `iex -S mix` on the terminal to open the console with this loaded project.

You can use the `process_input` function of module `JobQueues` to process a input file. You can put the input files into directory `inputs` and just pass the file name,
or you can pass the file name and the file path.

```elixir
iex> JobQueues.process_input("sample-input.json")
...
iex> JobQueues.process_input("another-input.json", "/home/jrgvf/Desktop/my-inputs")
...
```
The list of assigned jobs will be printed on the terminal.
```elixir
iex> JobQueues.process_input("sample-input.json")
[
  {
    "job_assigned": {
      "job_id": "c0033410-981c-428a-954a-35dec05ef1d2",
      "agent_id": "8ab86c18-3fae-4804-bfd9-c3d6e8f66260"
    }
  },
  {
    "job_assigned": {
      "job_id": "f26e890b-df8e-422e-a39c-7762aa0bac36",
      "agent_id": "ed0e23ef-6c2b-430c-9b90-cd4f1ff74c88"
    }
  }
]
:ok
```

You can process many files with aggregating the result, but if you want a clean execution, run:
```elixir
iex> JobQueues.clean()
:ok
```

If you're familiar with the Elixir `Map` syntax, you can use like this:

```elixir
iex> messages = [%{"new_agent" => %{"id" => "8ab86c18-3fae-4804-bfd9-c3d6e8f66260",
     "name" => "BoJack Horseman", "primary_skillset" => ["bills-questions"],
     "secondary_skillset" => []}},
 %{"new_job" => %{"id" => "f26e890b-df8e-422e-a39c-7762aa0bac36",
     "type" => "rewards-question", "urgent" => false}},
 %{"new_agent" => %{"id" => "ed0e23ef-6c2b-430c-9b90-cd4f1ff74c88",
     "name" => "Mr. Peanut Butter", "primary_skillset" => ["rewards-question"],
     "secondary_skillset" => ["bills-questions"]}},
 %{"new_job" => %{"id" => "690de6bc-163c-4345-bf6f-25dd0c58e864",
     "type" => "bills-questions", "urgent" => false}},
 %{"new_job" => %{"id" => "c0033410-981c-428a-954a-35dec05ef1d2",
     "type" => "bills-questions", "urgent" => true}},
 %{"job_request" => %{"agent_id" => "8ab86c18-3fae-4804-bfd9-c3d6e8f66260"}},
 %{"job_request" => %{"agent_id" => "ed0e23ef-6c2b-430c-9b90-cd4f1ff74c88"}}]
 ...
 iex> JobQueues.process_messages(messages)
 :ok
 iex> JobQueues.list_assigned_jobs()
  [
   {
     "job_assigned": {
       "job_id": "c0033410-981c-428a-954a-35dec05ef1d2",
       "agent_id": "8ab86c18-3fae-4804-bfd9-c3d6e8f66260"
     }
   },
   {
     "job_assigned": {
       "job_id": "f26e890b-df8e-422e-a39c-7762aa0bac36",
       "agent_id": "ed0e23ef-6c2b-430c-9b90-cd4f1ff74c88"
     }
   }
 ]
 :ok
 iex> JobQueues.clean()
 :ok
```

**Tests**

Run `mix test` on the terminal to run the  tests for this project.
```bash
$ mix test
....................................................[]
......[]
..........

Finished in 0.3 seconds
68 tests, 0 failures

Randomized with seed 325755
```