defmodule JobQueues.Job do
  alias Agent, as: ElixirAgent
  use ElixirAgent

  @doc """
  Starts JobQueues.Job.
  """
  def start_link do
    initial_state = for job_classification <- job_classifications(), into: %{}, do: {job_classification, %{}}
    ElixirAgent.start_link(fn -> initial_state end, name: __MODULE__)
  end

  @doc """
  Insert a Job identified by `id`.

  Returns `:ok`, `{:error, :invalid_job_id}`, `{:error, :invalid_job}` or `{:error, :job_already_exists}`
  """
  def insert(%{"id" => id, "urgent" => true} = job), do: insert_in_classification(id, job, :unassigned_urgent_jobs)
  def insert(%{"id" => id} = job), do: insert_in_classification(id, job, :unassigned_jobs)
  def insert(_), do: {:error, :invalid_job}

  @doc """
  Insert a Job identified by `id` in `job_classification`.

  Returns `:ok`, `{:error, :invalid_job_id}` or `{:error, :job_already_exists}`
  """
  def insert_in_classification(nil, _job, _job_classification), do: {:error, :invalid_job_id}
  def insert_in_classification(id, %{"id" => job_id} = job, job_classification) do
    if already_exists?(job_id) do
      {:error, :job_already_exists}
    else
      ElixirAgent.update(__MODULE__, fn (%{^job_classification => jobs} = state) ->
        Map.put(state, job_classification, Map.put(jobs, id, job))
      end)
    end
  end

  @doc """
  Recover a job by `id`.

  Returns a Map with `job_classification` and `Job` or empty.
  """
  def find(nil), do: %{}
  def find(id) do
    ElixirAgent.get(__MODULE__, fn (state) ->
      for {job_classification, jobs} <- state, job = find_job_by_id(jobs, id), into: %{} do
        {job_classification, job}
      end
    end)
  end

  @doc """
  Recover a job by `id` in `job_classification`.

  Returns a `Job` or `nil`.
  """
  def find_in_classification(nil, _job_classification), do: nil
  def find_in_classification(id, job_classification) do
    ElixirAgent.get(__MODULE__, fn (%{^job_classification => jobs}) -> find_job_by_id(jobs, id) end)
  end

  @doc """
  Recover all current classificated jobs.

  Returns a Map with current classificated jobs or empty.
  """
  def all do
    ElixirAgent.get(__MODULE__, fn (state) ->
      for {job_classification, jobs} <- state, into: %{} do
        {job_classification, Map.values(jobs)}
      end
    end)
  end

  @doc """
  Recover jobs by `job_classification`.

  Returns a List with jobs or empty.
  """
  def all(job_classification) do
    ElixirAgent.get(__MODULE__, fn (%{^job_classification => jobs}) -> Map.values(jobs) end)
  end

  @doc """
  Find an unnasigned job in `types`.

  Returns a Map with unnasigned classifications and jobs or nil.
  """
  def find_unassigned_job_in_types(types) do
    ElixirAgent.get(__MODULE__, fn (state) ->
      for {job_classification, jobs} <- state, (job_classification in unassigned_job_classifications()), into: %{} do
        {job_classification, find_job_in_types(jobs, types)}
      end
    end)
  end

  @doc """
  Find an assigned job by `agent_id`.

  Returns a Job or nil.
  """
  def find_assigned_job_by_agent_id(nil), do: nil
  def find_assigned_job_by_agent_id(agent_id) do
    ElixirAgent.get(__MODULE__, fn (%{assigned_jobs: jobs}) -> Map.get(jobs, agent_id) end)
  end

  @doc """
  Recover completed jobs by `agent_id`.

  Returns a List with completed jobs or empty.
  """
  def find_completed_jobs_by_agent_id(nil), do: []
  def find_completed_jobs_by_agent_id(agent_id) do
    ElixirAgent.get(__MODULE__, fn (%{completed_jobs: jobs}) ->
      jobs
      |> Map.values
      |> Enum.filter(fn(job) -> match?(%{"agent_id" => ^agent_id}, job) end)
    end)
  end

  @doc """
  Completes an assigned_job by `agent_id`.

  Returns `:ok`, `{:error, :invalid_agent_id}`, `{:error, :invalid_job}` or `{:error, :invalid_job_id}`.
  """
  def completes_assigned_job(nil, _job), do: {:error, :invalid_agent_id}
  def completes_assigned_job(_agent_id, nil), do: {:error, :invalid_job}
  def completes_assigned_job(_agent_id, %{"id" => nil}), do: {:error, :invalid_job_id}
  def completes_assigned_job(agent_id, %{"id" => job_id} = job) do
    delete_in_classification(agent_id, :assigned_jobs)
    insert_in_classification(job_id, job, :completed_jobs)
  end

  @doc """
  Delete a Job identified by `id`.

  Returns `:ok` or `nil`.
  """
  def delete_in_classification(nil, _job_classification), do: nil
  def delete_in_classification(id, job_classification) do
    ElixirAgent.update(__MODULE__, fn (%{^job_classification => jobs} = state) ->
      {_removed, jobs} = Map.pop(jobs, id)
      Map.put(state, job_classification, jobs)
    end)
  end

  @doc """
  Clean current jobs.

  Returns `:ok`.
  """
  def clean do
    state = for job_classification <- job_classifications(), into: %{}, do: {job_classification, %{}}
    ElixirAgent.update(__MODULE__, fn _current_state -> state end)
  end

  # Private Methods

  defp job_classifications, do: [:unassigned_urgent_jobs, :unassigned_jobs, :assigned_jobs, :completed_jobs]

  defp unassigned_job_classifications, do: [:unassigned_urgent_jobs, :unassigned_jobs]

  defp already_exists?(id), do: Enum.any?(job_classifications(), &find_in_classification(id, &1))

  defp find_job_by_id(jobs, id), do: Map.get(jobs, id) || Map.values(jobs) |> Enum.find(&(&1["id"] == id))

  defp find_job_in_types(jobs, types) do
    case Enum.find(jobs, fn({_id, %{"type" => type}}) -> (type in types) end) do
      {_id, job} -> job
      nil -> nil
    end
  end
end