defmodule JobQueues.Agent do
  alias Agent, as: ElixirAgent
  use ElixirAgent

  @doc """
  Starts JobQueues.Agent.
  """
  def start_link, do: ElixirAgent.start_link(fn -> %{} end, name: __MODULE__)

  @doc """
  Insert or update an Agent identified by `id`.

  Returns `:ok`, `{:error, :invalid_agent_id}` or `{:error, :invalid_agent}`
  """
  def insert_or_update(%{"id" => nil}), do: {:error, :invalid_agent_id}
  def insert_or_update(%{"id" => id} = agent), do: ElixirAgent.update(__MODULE__, &Map.put(&1, id, agent))
  def insert_or_update(_), do: {:error, :invalid_agent}

  @doc """
  Recover all current agents.

  Returns a list with current agents.
  """
  def all, do: ElixirAgent.get(__MODULE__, &(&1 |> Map.values))

  @doc """
  Recover agent by `id`.

  Returns an Agent or `nil`.
  """
  def find(nil), do: nil
  def find(id), do: ElixirAgent.get(__MODULE__, &Map.get(&1, id))

  @doc """
  Delete an Agent identified by `id`.

  Returns removed Agent or `nil`
  """
  def delete(nil), do: nil
  def delete(id), do: ElixirAgent.get_and_update(__MODULE__, &Map.pop(&1, id))

  @doc """
  Clean current agents.

  Returns `:ok`.
  """
  def clean, do: ElixirAgent.update(__MODULE__, fn _agents -> %{} end)
end