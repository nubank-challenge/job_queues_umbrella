defmodule JobQueues.Job.Requester do
  @doc """
  Find a fittest job for an agent, ends the assigned job, remove the job from respective queue and assign job.

  Returns `:ok` or `{:error, error_code}`
  """
  def request(nil), do: {:error, :invalid_agent_id}
  def request(agent_id) do
    agent_id
    |> JobQueues.Agent.find()
    |> find_fittest_job()
    |> assign_job(agent_id)
  end

  defp find_fittest_job(nil), do: {:error, :agent_not_found}
  defp find_fittest_job(%{"primary_skillset" => primary_skillset, "secondary_skillset" => secondary_skillset}) do
    find_unassigned_job_in_types(primary_skillset) || find_unassigned_job_in_types(secondary_skillset)
  end

  defp find_unassigned_job_in_types(types) do
    case JobQueues.Job.find_unassigned_job_in_types(types) do
      %{unassigned_jobs: nil, unassigned_urgent_jobs: nil} -> nil
      %{unassigned_jobs: job, unassigned_urgent_jobs: nil} -> %{unassigned_jobs: job}
      %{unassigned_jobs: nil, unassigned_urgent_jobs: job} -> %{unassigned_urgent_jobs: job}
      %{unassigned_jobs: _job, unassigned_urgent_jobs: job} -> %{unassigned_urgent_jobs: job}
    end
  end

  defp assign_job({:error, error}, _agent_id), do: {:error, error}
  defp assign_job(nil, agent_id) do
    assigned_job = JobQueues.Job.find_assigned_job_by_agent_id(agent_id)
    JobQueues.Job.completes_assigned_job(agent_id, ended_job_payload(assigned_job))
    {:error, :fittest_job_not_found}
  end

  defp assign_job(job_data, agent_id) do
    assigned_job = JobQueues.Job.find_assigned_job_by_agent_id(agent_id)
    JobQueues.Job.completes_assigned_job(agent_id, ended_job_payload(assigned_job))

    [{job_classification, %{"id" => job_id} = job}] = Map.to_list(job_data)
    JobQueues.Job.delete_in_classification(job_id, job_classification)
    JobQueues.Job.insert_in_classification(agent_id, assigned_job_payload(job, agent_id), :assigned_jobs)
  end

  defp ended_job_payload(nil), do: nil
  defp ended_job_payload(job), do: job |> Map.put("completed_at", DateTime.utc_now)

  defp assigned_job_payload(nil, _agent_id), do: nil
  defp assigned_job_payload(job, agent_id) do
    job
    |> Map.put("agent_id", agent_id)
    |> Map.put("assigned_at", DateTime.utc_now)
  end
end