defmodule JobQueues do
  use Application

  alias JobQueues.{Job, Agent}

  def start(_type, _args) do
    import Supervisor.Spec

    children = [
      worker(Agent, []),
      worker(Job, [])
    ]

    opts = [strategy: :one_for_all, name: JobQueues.Supervisor]
    Supervisor.start_link(children, opts)
  end

  @default_file_path "../../apps/job_queues/inputs/"
  @default_file_name "sample-input.json"

  @moduledoc """
  Here at Nubank we have a large Customer Experience team, focusing on delivering high quality, friendly support to our
  customers. To ensure the efficiency and success of this operation, we organize the work to be done in job queues.

  It's a version of a job queue.
  """

  @doc """
  This method process a input file and returns a JSON of assigned jobs.

  An error is returned if the file is does not exists or is not in expected format.

  ## Examples

      iex> JobQueues.process_input("file_not_fount.json")
      {:error, :enoent}

      iex> JobQueues.process_input("text_file.txt")
      {:error, :unprocessable_entity}

  """
  def process_input(file_name \\ @default_file_name, file_path \\ @default_file_path, print_in_stdout \\ true) do
    case File.read(file_path <> file_name) do
      {:ok, input_content} ->
        case Poison.decode(input_content) do
          {:ok, messages} when is_list(messages) ->
            process_messages(messages)
            list_assigned_jobs(print_in_stdout)
          _ ->
            {:error, :unprocessable_entity}
        end
      error -> error
    end
  end

  @doc """
  List all assigned jobs.

  Print a List of assigned jobs and returns `:ok` or return a List of assigned jobs.
  """
  def list_assigned_jobs, do: list_assigned_jobs(true)
  def list_assigned_jobs(true) do
    Job.all(:assigned_jobs)
    |> sort_jobs_by(:asc, "assigned_at")
    |> format_assigned_jobs()
    |> to_pretty_json()
    |> IO.puts()
  end
  def list_assigned_jobs(false) do
    Job.all(:assigned_jobs)
    |> sort_jobs_by(:asc, "assigned_at")
    |> format_assigned_jobs()
    |> to_json()
  end

  @doc """
  Clear all existing queues.

  Returns `:ok`
  """
  def clean do
    Agent.clean()
    Job.clean()
  end

  ## Private methods

  defp process_messages(messages), do: Enum.each(messages, &process_message/1)

  defp process_message(%{"new_agent" => agent}), do: Agent.insert_or_update(agent)
  defp process_message(%{"new_job" => job}), do: Job.insert(job)
  defp process_message(%{"job_request" => %{"agent_id" => agent_id}}), do: Job.Requester.request(agent_id)

  defp sort_jobs_by(jobs, :asc, attribute), do: jobs |> Enum.sort_by(&(&1[attribute]), &<=/2)
  
  defp format_assigned_jobs(jobs) do
    for %{"id" => id, "agent_id" => agent_id} <- jobs, do: %{"job_assigned" => %{"job_id" => id, "agent_id" => agent_id}}
  end

  defp to_pretty_json(jobs), do: jobs |> Poison.encode_to_iodata!(pretty: true)
  
  defp to_json(jobs), do: jobs |> Poison.encode!()

end