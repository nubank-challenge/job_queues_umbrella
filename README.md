# NuBank Challenge JobQueues

**Description**

This project was created for resolve a Nubank challenge part 1 and part 2.

This proposed solution consists in a umbrella project with 2 apps:

- [JobQueues](https://gitlab.com/nubank-challenge/job_queues_umbrella/tree/master/apps/job_queues) (Part 1).
- [JobQueuesWeb](https://gitlab.com/nubank-challenge/job_queues_umbrella/tree/master/apps/job_queues_web) (Part 2).

**Click on app link to see project details.*

**Setup**

This project was created using Elixir 1.5.2, to install follow the [instructions](https://elixir-lang.org/install.html) of official Elixir site.
Phoenix requires Node.js with npm, to install follow the [instructions](https://nodejs.org/en/download/package-manager/).

  * Install dependencies with `mix deps.get`
  * Install Node.js dependencies with `cd apps/job_queues_web && npm install && cd ../..`

**Tests**

Run `mix test` on the terminal to run the  tests for this project.
```bash
$ mix test
==> job_queues
...........................................................[]
...[]
......

Finished in 0.4 seconds
68 tests, 0 failures

Randomized with seed 973505
==> job_queues_web
....................................

Finished in 0.2 seconds
36 tests, 0 failures

Randomized with seed 973505
```

**Project Tree**

```bash
.
├── apps
│   ├── job_queues
│   │   ├── config
│   │   │   └── config.exs
│   │   ├── inputs
│   │   │   ├── sample-input.json
│   │   │   └── text_file.txt
│   │   ├── lib
│   │   │   ├── job_queues
│   │   │   │   ├── agent.ex
│   │   │   │   ├── job.ex
│   │   │   │   └── requester.ex
│   │   │   └── job_queues.ex
│   │   ├── mix.exs
│   │   ├── mix.lock
│   │   ├── README.md
│   │   └── test
│   │       ├── fixtures
│   │       │   ├── job-requests-more-than-jobs-input.json
│   │       │   ├── job-requests-more-than-jobs-output.json
│   │       │   ├── many-job-requests-of-an-agent-input.json
│   │       │   ├── many-job-requests-of-an-agent-output.json
│   │       │   ├── many-job-requests-of-an-agent-without-new-job-input.json
│   │       │   ├── many-job-requests-of-an-agent-without-new-job-output.json
│   │       │   ├── sample-output.json
│   │       │   ├── secondary-skillset-input.json
│   │       │   └── secondary-skillset-output.json
│   │       ├── job_queues
│   │       │   ├── agent_test.exs
│   │       │   ├── job_requester_test.exs
│   │       │   └── job_test.exs
│   │       ├── job_queues_test.exs
│   │       └── test_helper.exs
│   └── job_queues_web
│       ├── brunch-config.js
│       ├── config
│       │   ├── config.exs
│       │   ├── dev.exs
│       │   ├── prod.exs
│       │   ├── prod.secret.exs
│       │   └── test.exs
│       ├── lib
│       │   ├── job_queues_web
│       │   │   └── endpoint.ex
│       │   └── job_queues_web.ex
│       ├── mix.exs
│       ├── package.json
│       ├── priv
│       │   ├── gettext
│       │   │   ├── en
│       │   │   │   └── LC_MESSAGES
│       │   │   │       └── errors.po
│       │   │   └── errors.pot
│       │   └── static
│       │       ├── css
│       │       │   ├── app.css
│       │       │   └── app.css.map
│       │       ├── favicon.ico
│       │       ├── images
│       │       │   └── phoenix.png
│       │       ├── js
│       │       │   ├── app.js
│       │       │   └── app.js.map
│       │       └── robots.txt
│       ├── README.md
│       ├── test
│       │   ├── channels
│       │   ├── controllers
│       │   │   ├── agent_controller_test.exs
│       │   │   └── job_controller_test.exs
│       │   ├── support
│       │   │   ├── channel_case.ex
│       │   │   ├── conn_case.ex
│       │   │   └── conn_case_helper.ex
│       │   ├── test_helper.exs
│       │   └── views
│       │       ├── agent_view_test.exs
│       │       ├── error_view_test.exs
│       │       ├── job_view_test.exs
│       │       └── layout_view_test.exs
│       └── web
│           ├── channels
│           │   └── user_socket.ex
│           ├── controllers
│           │   ├── agent_controller.ex
│           │   └── job_controller.ex
│           ├── gettext.ex
│           ├── models
│           ├── router.ex
│           ├── static
│           │   ├── assets
│           │   │   ├── favicon.ico
│           │   │   ├── images
│           │   │   │   └── phoenix.png
│           │   │   └── robots.txt
│           │   ├── css
│           │   │   ├── app.css
│           │   │   └── phoenix.css
│           │   ├── js
│           │   │   ├── app.js
│           │   │   └── socket.js
│           │   └── vendor
│           ├── templates
│           │   ├── layout
│           │   │   └── app.html.eex
│           │   └── page
│           │       └── index.html.eex
│           ├── views
│           │   ├── agent_view.ex
│           │   ├── error_helpers.ex
│           │   ├── error_view.ex
│           │   ├── job_view.ex
│           │   └── layout_view.ex
│           └── web.ex
├── config
│   └── config.exs
├── mix.exs
├── mix.lock
└── README.md

41 directories, 78 files
```